# NTP

#### 项目介绍
    NTP 服务端 代替windows 自带Ntp服务自动停止问题
    NTP 客户端 UI界面和终端窗口 可使用本项目NTP服务时间源，能连接外网可使用阿里云NTP服务器
    阿里云Ntp服务地址：
                    ntp1.aliyun.com到ntp7.aliyun.com


#### 软件架构

            软件架构说明

            纯python编写

#### 安装教程

1. 二进制运行需要VC++ 2015运行库支持，请自行网上下载


#### 使用说明

1. 源码 使用python 3.65编写
2. 客户端 第三方库
    subprocess     运行系统命令
    ntplib         解析NTP服务端返回报文
    objconfig      读写ini配置文件
    QT5            UI库
3. 服务端 第三方库
    ntplib         封装NTP客户端报文

4. 二进制打包程序
	gui客户端使用 服务端时间源，地址为xx.xx.xx.xx：1234
	



