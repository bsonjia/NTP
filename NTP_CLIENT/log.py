#-*- coding:utf-8 -*-
import logging,re,os
from logging.handlers import TimedRotatingFileHandler
from logging.handlers import RotatingFileHandler
class Logger():
    def __init__(self,config):
        self.logger = logging.getLogger(__name__)
        #日志总等级
        self.level=int(config['level'])
        self.logger.setLevel(self.level)
        #定义handler的输出格式并将格式应用到handler
        self.formatter = logging.Formatter(" * %(asctime)s%(levelname)+8s:%(message)s")
        self.setConsoleLog()
        self.setSaveLog(config)
    #设置日志输出控制台
    def setConsoleLog(self):
        #控制台日志
        console_handler = logging.StreamHandler()
        #日志等级
        console_handler.setLevel(self.level)
        console_handler.setFormatter(self.formatter)
        self.logger.addHandler(console_handler)
    #设置日志存储到文件
    def setSaveLog(self,config):
        if config['saveLog']=='Y':
            logpath=os.path.abspath(config['logPath'])
            cycle=config['time'].split(":")
            try:
                os.mkdir(logpath)
                print(" * 日志文件夹创建成功")
            except :
                print(" * 日志文件夹已存在")
            log_file_handler = TimedRotatingFileHandler(filename=logpath+"/log", when="%s"%cycle[0], interval=int("%s"%cycle[1]), backupCount=int(config['saveNumber']))
            if cycle[0]=="D":
                log_file_handler.suffix = "%Y-%m-%d.log"
            elif cycle[0]=="H":
                log_file_handler.suffix = "%Y-%m-%d_%H.log"
            elif cycle[0]=="M":
                log_file_handler.suffix = "%Y-%m-%d_%H-%M.log"
            elif cycle[0]=="S":
                log_file_handler.suffix = "%Y-%m-%d_%H-%M-%S.log"
            log_file_handler.setFormatter(self.formatter)
            self.logger.addHandler(log_file_handler)

    def write(self,message='',level=10):
        message=str(message)
        #输出日志
        if level<=10:
            self.logger.debug(message)
        elif level<=20:
            self.logger.info(message)
        elif level<=30:
            self.logger.warning(message)
        elif level<=40:
            self.logger.error(message)
        elif level>40:
            self.logger.critical(message)
if __name__ == '__main__':
    info={
            'level':10,
            #是否存储日志
            'saveLog':'Y',
            #存储日志路径
            'logPath':'./log',
            #日志文件切割周期 D,H,M,S,天，时，分，秒
            'time':'H:1',
            #存储日志个数
            'saveNumber':168
        }
    Log = Logger(info)
    Log.write('chat xxxx',level=10)



# 模块级函数
#
# logging.getLogger([name]):返回一个logger对象，如果没有指定名字将返回root logger
# logging.debug()、logging.info()、logging.warning()、logging.error()、logging.critical()：设定root logger的日志级别
# logging.basicConfig():用默认Formatter为日志系统建立一个StreamHandler，设置基础配置并加到root logger中
#
# Loggers
#
# Logger.setLevel(lel):指定最低的日志级别，低于lel的级别将被忽略。debug是最低的内置级别，critical为最高
# Logger.addFilter(filt)、Logger.removeFilter(filt):添加或删除指定的filter
# Logger.addHandler(hdlr)、Logger.removeHandler(hdlr)：增加或删除指定的handler
# Logger.debug()、Logger.info()、Logger.warning()、Logger.error()、Logger.critical()：可以设置的日志级别

# Handlers
#
# handler对象负责发送相关的信息到指定目的地。可以通过addHandler()方法添加多个多handler
# Handler.setLevel(lel):指定被处理的信息级别，低于lel级别的信息将被忽略
# Handler.setFormatter()：给这个handler选择一个格式
# Handler.addFilter(filt)、Handler.removeFilter(filt)：新增或删除一个filter对象

# Formatters
#
# Formatter对象设置日志信息最后的规则、结构和内容，默认的时间格式为%Y-%m-%d %H:%M:%S，下面是Formatter常用的一些信息


# %(name)s                       Logger的名字
#
# %(levelno)s                    数字形式的日志级别
#
# %(levelname)s                文本形式的日志级别
#
# %(pathname)s                调用日志输出函数的模块的完整路径名，可能没有
#
# %(filename)s                  调用日志输出函数的模块的文件名
#
# %(module)s                    调用日志输出函数的模块名
#
# %(funcName)s                调用日志输出函数的函数名
#
# %(lineno)d                     调用日志输出函数的语句所在的代码行
#
# %(created)f                    当前时间，用UNIX标准的表示时间的浮 点数表示
#
# %(relativeCreated)d        输出日志信息时的，自Logger创建以 来的毫秒数
#
# %(asctime)s                  字符串形式的当前时间。默认格式是 “2003-07-08 16:49:45,896”。逗号后面的是毫秒
#
# %(thread)d                   线程ID。可能没有
#
# %(threadName)s           线程名。可能没有
#
# %(process)d                 进程ID。可能没有
#
# %(message)s               用户输出的消息