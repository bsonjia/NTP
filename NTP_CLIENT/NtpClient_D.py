#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 时间     : 2018-07-14 17:25:23
# 作者     : DL (584054432@qq.com)
# 网址     : https://gitee.com/dingliangPython/
# 软件版本 : Python3.6.5
# 功能     ：


import os,time,ntplib,sys,ReadConfig,threading,log,ToTime,subprocess

#互斥
# import win32event,win32con,win32api
# from winerror import ERROR_ALREADY_EXISTS
os.system("title NTP校时客户端")
class Main():
    """docstring for Main"""
    def __init__(self):
        super(Main, self).__init__()
        self.config=ReadConfig.Config()
        self.configInfo=self.config.read()
        self.logger=log.Logger(self.configInfo['Log'])
        self.host=self.configInfo['NTP']['host']
        self.port=int(self.configInfo['NTP'].get('port',123))
        self.version=int(self.configInfo['NTP'].get('version',2))

        self.log=self.logger.write
        self.ntp=ntplib.NTPClient()
    #校时函数
    def ntpStart(self):
        self.log("发送校时请求到：%s 校时NTP服务器"%self.host,20)
        response=self.ntp.request(self.host,port=self.port,version=self.version)
        ts = response.tx_time
        _date = time.strftime('%Y-%m-%d',time.localtime(ts))
        _time = time.strftime('%X',time.localtime(ts))
        subprocess.call('date {} && time {}'.format(_date,_time),shell=True)
        self.log("校时成功",20)
    def Run(self):
        while True:
            try:
                self.log("本机当前时间：%s"%ToTime.ToTime.getCurTime()[0],20)
                self.ntpStart()
            except Exception as e:
                self.log("校时失败：%s"%e,30)
            self.log("校时周期：%s"%self.configInfo["NTP"].get("Cycle",30))
            time.sleep(int(self.configInfo["NTP"].get("Cycle",30)))
if __name__ == '__main__':
    app=Main()
    app.Run()