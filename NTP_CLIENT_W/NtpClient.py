#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 时间     : 2018-07-14 17:25:23
# 作者     : DL (584054432@qq.com)
# 网址     : https://gitee.com/dingliangPython/
# 软件版本 : Python3.6.5
# 功能     ：

import subprocess
import os,time,ntplib,sys,gc
import Ui_GUI,ntp_rc,ReadConfig
from PyQt5.QtWidgets import QApplication,QMainWindow,QAction,QMenu,QSystemTrayIcon
from PyQt5.QtCore import pyqtSignal,QObject,QThread
from PyQt5.QtGui import QIcon
#互斥
# import win32event,win32con,win32api
# from winerror import ERROR_ALREADY_EXISTS
class Main(QThread):
    """docstring for Main"""
    def __init__(self):
        super(Main, self).__init__()
        self.app = QApplication(sys.argv)
        self.GUI = QMainWindow()
        self.ui = Ui_GUI.Ui_MainWindow()
        self.ui.setupUi(self.GUI)
        #点X并不关闭程序
        self.app.setQuitOnLastWindowClosed(False)
        self.menu()
        #锁定窗口
        self.GUI.setFixedSize(self.GUI.width(), self.GUI.height())
        #读取程序配置文件
        self.config=ReadConfig.Config()
        self.config.log.connect(self.setMsg)
        self.configInfo=self.config.read()

        #程序状态栏
        self.GUI.statusBar().showMessage('信息：')
        self.ui.SaveConfig.clicked.connect(self.saveConfig)
        self.AutoMode=self.configInfo['NTP']['Cycle']
        self.ui.AutoMode.stateChanged.connect(self.checkAutoMode)
        self.ui.Manual.clicked.connect(self.manualNtp)
        #加载配置
        self.loadConfigUi()
        #启动循环
        self.start()
        self.GUI.show()
        #显示托盘，没有这一步，退出不正常
        sys.exit(self.app.exec_())
    #循环线程函数
    def run(self):
        while 1:
            now=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            Cycle=int(self.configInfo['NTP']['Cycle'])
            t=int(time.time())%Cycle
            if self.AutoMode and t%Cycle ==0:
                try:
                    self.ntpStart()
                    self.setMsg("%s秒周期，自动校时执行成功,%s"%(Cycle,time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))
                except Exception as e:
                    self.setMsg("校时失败:%s"%e)
            self.ui.NowTime.setText(now)
            del now,Cycle,t
            gc.collect()
            time.sleep(1)
    #校时函数
    def ntpStart(self):
        ntp=ntplib.NTPClient()
        response=ntp.request(self.host, version=2, port=self.port, timeout=1)
        ts = response.tx_time
        _date = time.strftime('%Y-%m-%d',time.localtime(ts))
        _time = time.strftime('%X',time.localtime(ts))
        subprocess.call('date {} && time {}'.format(_date,_time),shell=True)
        del ntp
        gc.collect()
    #手动校时
    def manualNtp(self):
        try:
            self.ntpStart()
            self.setMsg("手动校时成功")
        except Exception as e:
            self.setMsg("手动校时失败：%s"%e)
    #输出状态栏
    def setMsg(self,value):
        self.GUI.statusBar().showMessage('信息：%s'%str(value))
        del value
        gc.collect()
    #加载配置文件
    def loadConfigUi(self):
        self.ui.Cycle.setValue(int(self.configInfo['NTP']['Cycle']))
        self.ui.NtpAddress.setText(self.configInfo['NTP']['host'])
        value=self.configInfo['NTP']['host'].split(':')
        if len(value)>=2:
            self.host=value[0]
            self.port=int(value[1])
        else:
            self.host=self.configInfo['NTP']['host']
            self.port=123
        if self.configInfo['NTP']['AutoMode']:
            self.ui.AutoMode.setChecked(True)
        else:
            self.ui.AutoMode.setChecked(False)
    #存储配置文件
    def saveConfig(self):
        c=self.configInfo['NTP']
        if self.ui.Cycle.text()!=c['Cycle']:
            self.config.write('NTP','Cycle',self.ui.Cycle.text())
        if self.ui.NtpAddress.text()!=c['host']:
            self.config.write('NTP','host',self.ui.NtpAddress.text())
        if self.AutoMode!= c['AutoMode']:
            self.config.write('NTP','AutoMode',self.AutoMode)
        self.configInfo=self.config.read()
    #复选存值
    def checkAutoMode(self,value):
        if value==2:
            self.AutoMode=True
            self.setMsg('开启自动校时')
        else:
            self.AutoMode=False
            self.setMsg('关闭自动校时')
    #退出
    def quit(self):
        #"保险起见，为了完整的退出"
        self.trayIcon.setVisible(False)
        #设备托盘非可见
        self.trayIcon.hide()
        #隐藏
        self.app.quit()
        #退出
        sys.exit()
    #托盘
    def menu(self):
        #系统托盘效果
        #菜单命令
        self.minimizeAction = QAction(u"最小化", self, triggered=self.GUI.hide)
        self.maximizeAction = QAction(u"最大化", self, triggered=self.GUI.showMaximized)
        self.restoreAction = QAction(u"还原大小", self, triggered=self.GUI.showNormal)
        self.tuichu=QAction(u"退出程序", self, triggered=self.quit)
        #增加托盘菜单命令
        self.IconMenu=QMenu(self.GUI)
        self.IconMenu.addAction(self.minimizeAction)
        self.IconMenu.addAction(self.maximizeAction)
        self.IconMenu.addAction(self.restoreAction)
        self.IconMenu.addAction(self.tuichu)
        #托盘图标
        self.trayIcon = QSystemTrayIcon()
        self.trayIcon.setIcon(QIcon(":/clock32.ico"))
        self.trayIcon.setToolTip("NTP校时工具")
        #托盘图标里添加菜单
        self.trayIcon.setContextMenu(self.IconMenu)
        self.trayIcon.activated.connect(self.iconShow)
        self.trayIcon.show()
    #还原托盘回窗口
    def iconShow(self,value):
        if value>=2:
           self.GUI.showNormal()

if __name__ == '__main__':
    # mutexname = "DEMO"#互斥体命名
    # mutex = win32event.CreateMutex(None, False, mutexname)
    # if (win32api.GetLastError() == ERROR_ALREADY_EXISTS):
    #     win32api.MessageBox(0,u'程序已运行，禁止重复启动！！！',u'警告',win32con.MB_OK)
    #     exit(0)
    app=Main()