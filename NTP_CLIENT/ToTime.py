#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 时间     : 2018-09-03 11:13:58
# 作者     : DL (584054432@qq.com)
# 网址     : https://gitee.com/dingliangPython/
# 软件版本 : Python3.6.5
# 功能     ：

import time,re
class ToTime(object):
    """输入任意格式时间，年月日时分秒转成utc时间戳，如2018 11 12 00:21:50 或2018-11-12 00:21:59
        年月日时分秒中间必须用任意符号空格等非数字分割
    """
    def __init__(self):
        super(ToTime, self).__init__()
    def strToUtc(s=None):
        try:
            if s and s.isdigit() and len(s)==14:
                ss="%s-%s-%s %s:%s:%s"%(s[0:4],s[4:6],s[6:8],s[8:10],s[10:12],s[12:14])
                return int(time.mktime(time.strptime(ss, '%Y-%m-%d %H:%M:%S')))
            elif s and re.findall('\d*.\d*.\d*.\d*.\d*.\d*',s):
                t=re.findall('\d+',s)
                t='%04d-%02d-%02d %02d:%02d:%02d'%tuple([int(v) for v in t])
                return int(time.mktime(time.strptime(t, '%Y-%m-%d %H:%M:%S')))

        except Exception as e:
            print("正确认格式为：年-月-日 时：分：秒/ 年月日时分秒，日期格式不正确认:%s"%e)
            return False
    def utcToDate(s=None):
        try:
            if s and len(str(s))>=10:
                t=str(s)
                return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(t[:10])))
        except Exception as e:
            print("正确认格式为：1970-1-1到现在的秒数，如：1535980488，日期格式不正确认:%s"%e)
            return False
    def getCurTime():
            return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())),int(time.time())
    def formatStrDate(s):
        try:

            if s and s.isdigit() and len(s)==14:
                ss="%s-%s-%s %s:%s:%s"%(s[0:4],s[4:6],s[6:8],s[8:10],s[10:12],s[12:14])
                return ss
            elif s and re.findall('\d*.\d*.\d*.\d*.\d*.\d*',s):
                t=re.findall('\d+',s)
                t='%04d-%02d-%02d %02d:%02d:%02d'%tuple([int(v) for v in t])
                return t
        except Exception as e:
            print("正确认格式为：年-月-日 时：分：秒，日期格式不正确认:%s"%e)
            return False
    def unsignedDataTime(datatime):
        return ''.join(re.findall('\d+',datatime))
if __name__ == '__main__':


    # print(ToTime.utcToDate(1535945151))
    # print(ToTime.getCurTime())
     print(ToTime.formatStrDate('201/9/3 1,9,6'))
    # print(ToTime.formatStrDate('2017:7/9:1:25,33'))

#print time.mktime(time.strptime(times, '%Y-%m-%d %H:%M:%S'))